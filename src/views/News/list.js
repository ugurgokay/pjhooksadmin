import React, { useState, useEffect } from 'react';
import axios from 'axios'

import {Table, Button,Modal} from 'react-bootstrap';

import CreateNewsForm from '../../components/forms/createNews';

var eP = require('../../constants.js')

export default function NewsList({}){

    const [cats, setCats] = useState([]);
    const [showModal, setShowmOdal] = useState(false);

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    
      const open = () =>   {
        setShowmOdal(true);
      }
      
 
      useEffect(() => {
        axios.get(eP.apiUrl +  eP.news)
          .then(res => {
            console.log(res)
            setCats(res.data)
          })
      
    }, []);

      // render(){
        // const { cats } = cats;
        const catsList = cats.length ? (
        cats.map(cat => {
          return (
            <tr key={cat.id}>
                <td>
                    <img src={cat.coverImage}  style={{height : "135px"}}/>
                </td>
                <td>
                <b>{cat.header}</b>  <br/> {cat.description}
                </td>
                <td>
                    {cat.context}
                </td>
            </tr>
          )        
          })
        ) : (
          <div className="center">No Category Yet</div>
        )
        return(
          <div className="container">
            <Button className="offset-sm-9 col-sm-3" onClick={() => handleShow() } >Yeni Haber Ekle</Button>

            <h4> Haberler Listesi </h4>
            
             <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Sipariş Edilen Ürünler</th>
                    <th>Sipariş Tarihi</th>
                    </tr>
                </thead>
                <tbody>
                        {catsList}
                </tbody>
            </Table>
            
            <Modal show={show} onHide={handleClose} size="lg">
                  <Modal.Header closeButton>
                    <Modal.Title>Yeni Haber Ekle</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <CreateNewsForm cats={'a'} />
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                      Close
                    </Button>
                    {/* <Button variant="primary" onClick={handleClose}>
                      Save Changes
                    </Button> */}
                  </Modal.Footer>
                </Modal>
        </div>   
    )
}

