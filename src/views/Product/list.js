import React, { useState,useEffect } from 'react';
import axios from 'axios'

import {Form,Row, Col, Button,Modal,Table} from 'react-bootstrap';
import CreateProductForm from '../../components/forms/createProduct';

import ProductDetail from './detail';

var eP = require('../../constants.js')


export default function ProductList({}) {

  const [feats, setFeats] = useState([])

  const [prdDetail, setPrdDetail] = useState();
  
  const [show, setShow] = useState(false);
  const [showDetail, setShowDetail] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleDetailClose = () => setShowDetail(false);
  const handleDetailShow = (node) => {
    
    setShowDetail(true)
    console.log(node)

    setPrdDetail(node)
    console.log(prdDetail)
  }

  const [allCategories, setAllCategories] = useState([]);
  const [selectedCat, setSelectedCat] = useState();

  useEffect(() => {
    axios.get(eP.apiUrl +  eP.subCategories)
      .then(res => {
        setAllCategories(res.data)
      })	


}, []);




      useEffect(() => {
        axios.get(eP.apiUrl +  eP.products,
          { 'headers': { 'Authorization': 'Bearer ' + localStorage.getItem('token') } })
          .then(res => {
            setFeats(res.data)
          })	
      
    }, []);


      const ProductFeatures = ({feat}) => {
  
        let ft = JSON.parse(feat)

        return (
          <div>
          {ft && ft.map(item => {
              return <div key={item.id}>{item.productType}</div>;
            })}
        </div>
        )
      }
        const featsList = feats.length ? (
        feats.map(feat => {
            return (
                <tr key={feat.id}>
                    <td>  # { /*feat.id */}  </td>
                    <td> <span> {feat.productName}</span>  </td>
                    {/* <td> {feat.relatedCategoryId} </td> */}
                    <td> 
                      <ProductFeatures feat={feat.productTypes} />
                    </td>
                    <td> <img src={feat.picture} style={{height : "135px"}} /> </td>
                    <td> {feat.quantity }</td>
                    <td> {feat.price} $</td>
                    <td> 
                    <Button onClick={()=> handleDetailShow(feat)}> Detay</Button>
                    </td>
                </tr>
            )        
            })
          ) : (
              <tr>
            <td colSpan="4" className="center">No Order Yet</td>
                </tr>
          )
          const handleClick = (item) => {
            axios.get(eP.apiUrl +  eP.products +'/'+  item,
          { 'headers': { 'Authorization': 'Bearer ' + localStorage.getItem('token') } })
            .then(res => {
              setFeats(res.data)
            }).catch(err => {
              if(err.response.status == 401)
              {
                localStorage.removeItem('user')  
                localStorage.removeItem('token')
                 window.location.href = '/login'
              }
            })	
          };
          return( 
                <div className="container">
                    <Button className="offset-sm-9 col-sm-3" onClick={() => handleShow() } >Yeni Ürün Ekle</Button>
                    <h4> Ürün Listesi </h4>
                    <p>
                        <i>
                            Ürünleri burada görüntüleyebilirsiniz
                        </i>
                    </p>
                    <p>
                        {allCategories.map((item, index) => 
                          <Button style={{margin:'2px', padding:'1px'}} onClick={() => handleClick(item.id)} key={index}>{item.mainCat} - {item.title}</Button>
                        )
                        }
                    </p>
                    <div className="row" style={{overflowX: 'scroll'}}>
                    
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>Ürün Adı</th>
                            {/* <th>Kategorisi</th> */}
                            <th>Ürün Özellikleri</th>
                            <th>Fotoğraf</th>
                            <th>Aktif Stok</th>
                            <th>Fiyat</th>
                            <th>Ürün İşlemleri</th>
                            </tr>
                        </thead>
                        <tbody>
                                {featsList}
                        </tbody>
                    </Table>
                    </div>

                      <>
                <Modal show={show} onHide={handleClose} size="lg">
                  <Modal.Header closeButton>
                    <Modal.Title>Yeni Ürün Ekle</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <CreateProductForm cats={'a'} />
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                      Close
                    </Button>
                    {/* <Button variant="primary" onClick={handleClose}>
                      Save Changes
                    </Button> */}
                  </Modal.Footer>
                </Modal>
                <Modal show={showDetail} onHide={handleDetailClose} size="lg">
                  <Modal.Header closeButton>
                    <Modal.Title>Yeni Ürün Ekle</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <ProductDetail cats={prdDetail} />
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={handleDetailClose}>
                      Close
                    </Button>
                  </Modal.Footer>
                </Modal>
              </>

                </div>   
            )
        // }
    }


// export default ProductList;