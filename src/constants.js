'use strict';

const { Container } = require("react-bootstrap");

var CONSTANTS= function(){ }

CONSTANTS.apiUrl = 'http://localhost:3001/';


//  CONSTANTS.apiUrl = 'http://185.224.139.140:3001/'
//  CONSTANTS.apiUrl = 'http://185.224.139.140:3002/'


// CONSTANTS.apiUrl = 'http://192.168.1.1git04:3001/'
// CONSTANTS.apiUrl = 'http://185.224.139.140:3002/'




// CONSTANTS.apiUrl = 'http://192.168.43.208:3001/'

// CONSTANTS.apiUrl = 'https://pjhooksapi.azurewebsites.net/'

// CONSTANTS.apiUrl = 'http://192.168.43.208:3001/';
// TODO CONSTANTS.port = '3001';
CONSTANTS.categories = 'categories';
CONSTANTS.subCategories = 'subCategories';
CONSTANTS.products = 'products';
CONSTANTS.categoriesFeatures = 'categoriesFeatures';
CONSTANTS.user = 'user';
CONSTANTS.users = 'users';
CONSTANTS.news = 'news';

CONSTANTS.order = 'order';
CONSTANTS.orders = 'orders';
CONSTANTS.productDetail = 'productDetail';
CONSTANTS.updateUserStatus = 'updateUserStatus';

module.exports = CONSTANTS;
