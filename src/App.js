import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import './App.css';

import AppNavigationBar from './components/Aside'


import Home from './components/Home'
import CategoryList from './views/Category/list'
import CategoryFeatures from './views/Category/feature'
import ProductList from './views/Product/list'
import Orders from './views/order'
import Users from './views/user'
import News from './views/News/list'

import Login from './views/login'

import {PrivateRoute} from './components/PrivateRoute'
// import Settings from './views/settings'


// TODO
// AUTH login
// if sold mark as sold and decrease the stock Limit
// adminde ana sayfada onay bekleyen siparişler diye bir bölümde olmalıdır
function App() {
  return (
    <BrowserRouter>
      <div className="App gaming">
        <AppNavigationBar></AppNavigationBar>

        <div className="container content-holder">
          <div className="row" >
            <div className="col-sm-12">
                <Route path="/login" component={Login} />
                <PrivateRoute exact path="/" component={Home}  />
                <PrivateRoute path="/CategoryList" component={CategoryList}  />
                <PrivateRoute path="/CategoryFeatures" component={CategoryFeatures} />
                <PrivateRoute path="/ProductList" component={ProductList} />
                <PrivateRoute path="/Orders" component={Orders} />
                <PrivateRoute path="/Users" component={Users} />
                <PrivateRoute path="/News" component={News} />
              </div>
          </div>
        </div>
          
     


      </div>
    </BrowserRouter>
  );
}

export default App;
