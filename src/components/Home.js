import React, { Component,useState,useEffect } from 'react';
import logo from '../logo.svg';
import axios from 'axios';

var eP = require('../constants.js')

  export default function Home({}){

  const [posts , setPosts] = useState([])
  const [users , setUsers] = useState([])

  useEffect(() => {
    axios.get(eP.apiUrl +  eP.order,
      { 'headers': { 'Authorization': 'Bearer ' + localStorage.getItem('token') } })

    .then(res => {
      console.log(res.data)
      setPosts(res.data)
    })

    axios.get(eP.apiUrl +  eP.users,
      { 'headers': { 'Authorization': 'Bearer ' + localStorage.getItem('token') } })

    .then(res => {
      // console.log(res.data)
      setUsers(res.data)
    }).catch(err => {
      if(err.response.status == 401)
      {
        localStorage.removeItem('user')  
        localStorage.removeItem('token')
         window.location.href = '/login'
      }
    })
  },[])

 







    // render(){
    // const { posts,users } = this.state;
    
    const postList = posts.length ? (
      posts.map(post => {
      return (
        <div className="post card" key={post.id}>
          <div className="card-content">
            {post.name} - {post.createTime}
          </div>
        </div>
      )        
      })
    ) : (
      <div className="center">No Category Yet</div>
    )

    // const usersList = users.length ? (
    //   users.map(user => {
    //   return (
    //     <div className="post card" key={user.id}>
    //       <div className="card-content">
    //         {user.name} 
    //       </div>
    //     </div>
    //   )        
    //   })
    // ) : (
    //   <div className="center">No User Yet</div>
    // )


    return (
        <div>
        <div className="row">
          <div className="col-sm-8">
            <h2>Onay Bekleyen Siparişler</h2>
            <div className="card">
              {postList}
            </div>
          </div>
          {/* <div className="col-sm-4">
            <h2>Yeni Üyeler</h2>
            <blockquote className="card">
              {usersList}
            </blockquote>
          </div> */}
        </div>
        </div>
        ) 

    // }
  }

// export default Home;