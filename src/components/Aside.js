import React from 'react';

import {Navbar,Nav,NavDropdown} from 'react-bootstrap'

// import Settings from '../views/settings'

  const pushLogout = () => {
    localStorage.removeItem('user')  
    localStorage.removeItem('token')
    window.location.href = '/login'
  }

const AsideBar = () => {

    
  console.log(localStorage.getItem('user'))
  if(localStorage.getItem('user')){
    return (
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Navbar.Brand href="/">Romaster</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
    
    
    
        <Nav className="mr-auto"><Nav.Link href="/ProductList"> Ürünler</Nav.Link>
          <Nav.Link href="/Orders">Siparişler</Nav.Link>
          <Nav.Link className={[localStorage.getItem('user') == 'user' ? 'hide' : '' ]}   href="/Users">Üyeler</Nav.Link>
          <Nav.Link href="/CategoryList">Kategoriler</Nav.Link>
          <Nav.Link href="/News">Haberler</Nav.Link>
        </Nav>
        <Nav>
          {/* <Nav.Link href="#deets">Ayarlar</Nav.Link> */}
          <Nav.Link onClick={ pushLogout}>
            Çıkış
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
  }else {
    return (
      <></>
      )
  }

  

}

export default AsideBar;