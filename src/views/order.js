import React, { useState,useEffect } from 'react';
import axios from 'axios'
import Moment from 'react-moment';
import OrderDetail from '../components/order/orderDetail'
import {Table,Button,Modal} from 'react-bootstrap'
var eP = require('../constants.js')


export default  function Orders({})  {

    const [feats, setFeats] = useState([])


    const [selectedOrderDetail, setSelectedOrderDetail] = useState();

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    useEffect(() => {
        axios.get(eP.apiUrl +  eP.order,
          { 'headers': { 'Authorization': 'Bearer ' + localStorage.getItem('token') } })
          .then(res => {
            setFeats(res.data)
          })
    }, []);





const prepareOrderDetail = (detail) => {
    setSelectedOrderDetail(detail)
    // console.log(detail)
    handleShow()
}

const ProductType = ({type}) => {
    return (
        <div> 
        {type && type.map(node => {
            return (
              <div className={[node.value == 0 ? 'hide':'shown']} key={node.id}> Sipariş Bedeli :  {parseInt(node.productTotalPrice * node.productSubPiece * node.value)} $ </div>
          )
        })} 
        </div>
    )
}

const OrderDetails = ({feat}) => {
    let node = JSON.parse(feat)
    return (
        <div>
        {node && node.map(item => {
            return <><div key={item.id}>Ürün Adı : {item.productName}  <ProductType type={item.productTypes} /> </div> <hr/></>;
          })}
      </div>
    )
}


const featsList = feats.length ? (
        feats.map(feat => {
            return (
                <tr key={feat.id}>
                    <td>{feat.name}</td>
                    <td> 
                        <OrderDetails feat={feat.orderDetail} />
                    </td>
                    <td> <Moment format="D MMM YYYY HH:mm" withTitle>{feat.createTime}</Moment>  </td>
                    <td> 
                    {feat.orderType}
                    <br />
                    <Button
                    onClick={() =>  prepareOrderDetail(feat) }>
                        Detay
                    </Button> </td>

                </tr>
            )        
            })
          ) : (
              <tr>
            <td colSpan="4" className="center">No Order Yet</td>
                </tr>
          )

          

            return( 
                <>
                <div className="container">
                    <h4> Siparişler Listesi </h4>
                    <p>Onaysız Siparişler</p>
                    <div className="row">
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>Sipariş Edilen Ürünler</th>
                            <th>Sipariş Tarihi</th>
                            <th>Sipariş İşlemleri</th>
                            </tr>
                        </thead>
                        <tbody>
                                {featsList}
                        </tbody>
                    </Table>
                    </div>
                </div>   
                <>
                <Modal show={show} onHide={handleClose} size="lg">
                  <Modal.Header closeButton>
                    <Modal.Title>Yeni Ürün Ekle</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <OrderDetail cats={selectedOrderDetail} />
                  </Modal.Body>
                  {/* <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                      Close
                    </Button>
                    <Button variant="primary" onClick={handleClose}>
                      Save Changes
                    </Button>
                  </Modal.Footer> */}
                </Modal>
              </>
                </>
            )
    }

