import React, {useState} from 'react';
import { Formik } from 'formik';
import axios from 'axios';
import {Form,Button,Row,Col} from 'react-bootstrap';

var eP = require('../../constants.js')

export default function CreateCategoryForm({categories}){
  // console.log('--',categories)

  const [image,setImage] =useState();
  const [imagePath, setImagePath] = useState();
  let handleFileChange = (e) => {
    if (!e.target.files) {
      return;
    }
    let file = e.target.files[0];
    // console.log(file)

    setImage(file);
    let data = new FormData();
    data.append('file', image);
    const config = { /*headers: { 'Content-Type': 'multipart/form-data' } */};
    let fd = new FormData();
    fd.append('file',file)
    // let res =  axios.post(eP.apiUrl + 'upload', fd, config)




//todo ALSO _thumb

    axios({
      method: 'post',
      url: eP.apiUrl + 'upload',
      data: fd,
      headers: {'Content-Type': 'multipart/form-data' }
      })
      .then(function (response) {
          //handle success
          console.log(response);
          setImagePath(response.data.filename)
      })
      .catch(function (response) {
          //handle error
          console.log(response);
      });

  }


return (
  <div>
    <Formik
      initialValues={{ title: ''}}
      onSubmit={(values, actions) => {
        setTimeout(() => {
          console.log(JSON.stringify(values, null, 2));

          values.mainCategoryId = !values.mainCategoryId ? values.mainCategoryId = 0  : values.mainCategoryId

                axios.post(eP.apiUrl +  eP.categories, {
                  title: values.title,
                  title_ru: 'title_ru',
                  descr: 'descr_en',
                  descr_ru: 'descr_ru',
                  image :  'http://185.224.139.140/romasterApi/uploads/images/' +  imagePath,
                  categoryOrder: values.categoryOrder,
                  mainCategoryId : values.mainCategoryId
                })
                .then(function (response) {
                  console.log(response);
                  window.location.reload();
                //   debugger;
                })
                .catch(function (error) {
                  console.log(error);
                  alert('Bir Hata Oluştu Lütfen tekrar deneyiniz')
                //   debugger;
                });

          actions.setSubmitting(false);
        }, 1000);
      }}
    >
      {props => (
        <Form onSubmit={props.handleSubmit} className="clearfix">
            <Row>
                <Col sm={6}>
                    <Form.Group controlId="formTitle">
                        <Form.Label>Kategori Adı - EN</Form.Label>
                        <Form.Control  placeholder="İngilizce Kategori Adı"
                        type="text"
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        value={props.values.title}
                        name="title"
                        required />
                    </Form.Group>
                </Col>
                {/* <Col sm={6}>
                    <Form.Group controlId="formTitle_ru">
                        <Form.Label>Kategori Adı - RU</Form.Label>
                        <Form.Control  placeholder="Rusça Kategori Adı"
                                type="text"
                                onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                value={props.values.title_ru}
                                name="title_ru"
                                required />
                    </Form.Group>
                </Col> */}
            </Row>
            <Row>
                <Col sm={6} xs={12}>

                    <Form.Group controlId="imageUrl">
                        <Form.Label>Resim Yolu</Form.Label>
                        <Form.File 
                      id="custom-file"
                      label="Custom file input"
                      // onChange={props.handleChange}
                      onChange={handleFileChange}
                      onBlur={props.handleBlur}
                      value={props.values.picture}
                      custom
                      name="picture" 
                    />

                    </Form.Group>


                </Col>
                <Col  sm={6} xs={12}>
                <Form.Group controlId="kategori.secimi">
                    <Form.Label>Bağlı Olduğu Kategori</Form.Label>
                    <Form.Control as="select" onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                value={props.values.relatedCategoryId}
                                name="mainCategoryId" 
                                required>
                                <option selected disabled> Lütfen Seçim Yapınız</option>
                                   {
                                  categories.map((item, index) => 
                                    <option  value={item.id} key={index}>{item.title_ru} - {item.title}</option>
                                  )
                                  }

                   
                    </Form.Control>
                </Form.Group>
                </Col>
                <Col  sm={6} xs={12}>
                        <Form.Group controlId="order">
                        <Form.Label>Sıra</Form.Label>
                            <Form.Control placeholder="Sıra"
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.categoryOrder}
                                    name="categoryOrder"
                                    required />
                        </Form.Group>
                </Col>
            </Row>
            <Button variant="primary float-sm-right" type="submit">Kaydet</Button>
        </Form>
      )}
    </Formik>
  </div>
);
}
// export default CreateCategoryForm;
