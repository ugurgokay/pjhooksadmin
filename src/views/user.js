import React, { useEffect,useState } from 'react';
import axios from 'axios'
import {Table,Button,Modal} from 'react-bootstrap'
import Moment from 'react-moment';
import UserDetail from '../components/user/detail'
import { useHistory } from "react-router-dom";

// TODO
// kullanıcı blokla gibi bir özellik ekle
// her şifre gönder dediğinde 
// number of attemptsi 1 artır

var eP = require('../constants.js')

export default  function Users({})  {
  let history = useHistory();

    // class Users extends Component{
    // state = {
    //     feats: []
    //   }
    const [feats, setFeats] = useState([])
    const [feat, setFeat] = useState()

// const CategoryFeatures = () => {
// console.log(moment().format('2000')  );
  if(localStorage.getItem('user') == 'user')
      history.push('/')



    const userDetail = (user) => {
        setFeat(user);
        handleShow()

    };
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    useEffect(() => {
        // axios.get('https://jsonplaceholder.typicode.com/posts')
        axios.get(eP.apiUrl + eP.users,
          { 'headers': { 'Authorization': 'Bearer ' + localStorage.getItem('token') } })
          .then(res => {
            // console.log( new Date(res.data[0].createDate)) 
            setFeats(res.data)
          }).catch(err => {
            if(err.response.status == 401)
            {
              localStorage.removeItem('user')  
              localStorage.removeItem('token')
               window.location.href = '/login'
            }
          })
      }, [] );



        // const { feats } = this.state;
        const featsList = feats.length ? (
        feats.map(feat => {
            return (
                <tr key={feat.id} className={ feat.status ? 'activeUser' : 'deactiveUser'} style={{backgroundColor: 'rgba(0,0,0,.075)'}}> 
                    <td>{feat.name} {feat.surname}  </td>
                    <td> {feat.countryCode} </td>
                    <td> {feat.phoneNumber} </td>
                    <td> {feat.eMail} </td>
                    <td> <Moment format="D MMM YYYY HH:mm" withTitle>{feat.createDate}</Moment> </td>
                    <td> <Moment fromNow>{feat.lastOnline}</Moment> </td>
                    {/* <td> 5 </td> */}
                    <td> 
                        <Button
                    onClick={() =>  userDetail(feat) }>
                        Detay
                    </Button> 
                    {/* <Button>Sipariş Geçmişi</Button> */}
                        
                        
                        {/* </span> */}
                    </td>
                </tr>
            )        
            })
          ) : (
              <tr>
            <td colSpan="7" className="center">No User Yet</td>
                </tr>
          )

        //   const dateToFormat = '1976-04-19T12:59-0500';
        //   const dateToFormat = '2020-05-23T14:41:23.000Z'
          

            return( 
                <>
                <div className="container">
            {/* <Moment fromNow>{dateToFormat}</Moment> */}
                    <h4> Üye Listesi </h4>
                    <div className="row" style={{overflowX: 'scroll'}}>
                    <Table  bordered hover>
                        <thead>
                            <tr >
                            {/* <th>#</th> */}
                            <th>Kişisel Bilgiler</th>
                            <th>Ülke</th>
                            <th>Telefon</th>
                            <th>E-posta</th>
                            <th>Kayıt Tarihi</th>
                            <th>Son Giriş Tarihi</th>
                            {/* <th># of Attempt</th> */}
                            <th>Detay</th>
                            </tr>
                        </thead>
                        <tbody>
                                {featsList}
                        </tbody>
                    </Table>
                    </div>
                </div>   
                <Modal show={show} onHide={handleClose} size="lg">
                  <Modal.Header closeButton>
                    <Modal.Title>Konumu</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <UserDetail cats={feat} />
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                      Close
                    </Button>
                    <Button variant="primary" onClick={handleClose}>
                      Save Changes
                    </Button>
                  </Modal.Footer>
                </Modal>
                </>
            )
        }
    // }


// export default Users;