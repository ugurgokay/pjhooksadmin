import React, {useEffect,useState,ChangeEvent} from 'react';
import { Formik,Field,FieldArray } from 'formik';
import axios from 'axios';
import {Form,Button,Row,Col} from 'react-bootstrap';

var eP = require('../../constants.js')


// const CreateProductForm = () => (
export default function CreateProductForm({cats}){
  const [allCategories, setAllCategories] = useState([])  

  const [image,setImage] =useState();
  const [imagePath, setImagePath] = useState();

  const [lastProduct, setLastProduct] = useState({})
  const [lastPFeature, setLastPFeature] = useState({})
  const [lastProductInfo, setLastProductInfo] = useState({})



  const [imageLoading, setImageLoading] = useState(false);
  let handleFileChange = (e) => {
    if (!e.target.files) {
      return;
    }
    let file = e.target.files[0];
    // console.log(file)

    setImage(file);
    let data = new FormData();
    data.append('file', image);
    const config = { /*headers: { 'Content-Type': 'multipart/form-data' } */};
    let fd = new FormData();
    fd.append('file',file)
    // let res =  axios.post(eP.apiUrl + 'upload', fd, config)


    setImageLoading(true);


    axios({
      method: 'post',
      url: eP.apiUrl + 'upload',
      data: fd,
      headers: {'Content-Type': 'multipart/form-data' }
      })
      .then(function (response) {
          //handle success
          console.log(response);
          setImagePath(response.data.filename)
          setImageLoading(false);

      })
      .catch(function (response) {
          //handle error
          console.log(response);
      });

  }

  
  useEffect(() => {
      axios.get(eP.apiUrl +  eP.subCategories)
      // axios.get('http://localhost:3001/products/6')
        .then(res => {
          // console.log(res.data)
          setAllCategories(res.data)
        // console.log(allCategories);
        })	

  
}, []);

useEffect(() => {
  axios.get(eP.apiUrl +  'lastProduct')
    .then(res => {

      setLastProduct(res.data[0])

      let lastFeature = JSON.parse(res.data[0].productFeatures)
      setLastPFeature(lastFeature.data)
      // console.log(typeof res.data[0].productFeatures)

      let lastProductInf = JSON.parse(res.data[0].productTypes)

      setLastProductInfo(lastProductInf[0])
      // console.log(lastProductInfo)

    })	

}, []);



const initialProductFields = [{"item": "CLOTH",  "value": "%80 Wool, %20 Polymer"},{"item": "FIT",  "value": "SLIM FIT"},{"item": "DROP",  "value": "6"},{"item": "SERİ",  "value": "48-58"}]
// const initialProductFields = [{"item": "LANING",  "value": "ASETAT"},{"item": "CLOTH",  "value": "%86 Wool, %10 Polymer"},{"item": "FIT",  "value": "SLIM FIT"},{"item": "DROP",  "value": "6"},{"item": "SERİ",  "value": "46-56"}]


  return(
    <>
    { imageLoading ? <div className="loadingBar"> RESİM YÜKLENİYOR </div> : ''}

  <div>
          {/* {cats}
        --- */}
          {/* {allCategories} */}

    <Formik
      initialValues={{ relatedCategoryId: lastProduct.relatedCategoryId, data : lastPFeature , productName: lastProduct.productName, categoryOrder:1 , pF: [], pf_1_value: 'AHAH', featureOneName: lastProductInfo.productType, featureOneQuantity: lastProductInfo.productSubPiece, featureOneStock: lastProductInfo.quota, featureOnePrice : lastProductInfo.productTotalPrice  }}
      // initialValues={{ data : initialProductFields , productName: lastProduct.productName, categoryOrder:1 , pF: [], pf_1_value: 'AHAH'}}
      enableReinitialize={true}
      onSubmit={(values, actions) => {
        setTimeout(() => {
          console.log(JSON.stringify(values, null, 2));

          // console.log(typeof initialProductFields)
          
                if(values.relatedCategoryId == undefined){
                    alert('Lütfen Kategori Seçimi Yapınız')
                    return;
                }




                let subProducts = [];
                let obj1 = {
                  "id" : 1,
                  "productType":values.featureOneName,
                  "productTotalPrice" : values.featureOnePrice,
                  "productSubPiece" : values.featureOneQuantity,
                  "quota" : values.featureOneStock,
                  "value" : ""
                  // "quantity": subPrOneQuantity
                  }




                  subProducts.push(obj1)

                  if(values.featureTwoName != undefined ){
                  let obj2 = {
                  "id": 2,
                  "productType":values.featureTwoName,
                  "productTotalPrice" : values.featureTwoPrice,
                  "productSubPiece" : values.featureTwoQuantity,
                  "quota" : values.featureTwoStock,
                  "value" : ""
                  // "quantity": subPrTwoQuantity
                    }
                    subProducts.push(obj2)
                  }
                  values.productTypes = JSON.stringify(subProducts);

                let stockFeautre = values.featureOneStock
                if(values.featureTwoStock != undefined){
                  stockFeautre = parseInt(values.featureOneStock) + parseInt(values.featureTwoStock)
                }


                axios.post(eP.apiUrl +  eP.products, {
                  productName: values.productName,
                  productName_ru:  values.productName, //values.productName_ru,
                  relatedCategoryId : values.relatedCategoryId,
                  picture :  'http://185.224.139.140/romasterApi/uploads/images/' +  imagePath,
                  productFeatures : JSON.stringify({"data" : values.data}),
                  productTypes : JSON.stringify(subProducts),
                  quantity: stockFeautre,
                  price : values.featureOnePrice,
                  status : true
                })
                .then(function (response) {
                  console.log(response);
                  window.location.reload();
                //   debugger;
                })
                .catch(function (error) {
                  console.log('e///', error);
                  alert('Bir Hata Oluştu Lütfen tekrar deneyiniz')
                //   debugger;
                });

          // actions.setSubmitting(false);
        }, 1000);
      }}
    >
      {props => (
        <Form onSubmit={props.handleSubmit} className="clearfix">
            <Row>
                <Col sm={6} xs={12}>
                    <Form.Group controlId="productName">
                        <Form.Label>Ürün Adı</Form.Label>
                        <Form.Control  placeholder="İngilizce Ürün Adı"
                        type="text"
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        value={props.values.productName}
                        name="productName" />
                    </Form.Group>
                </Col>
            </Row>
            <Row>
                <Col sm={6} xs={12}>
                    <Form.Group controlId="imageUrl">
                        <Form.Label>Resim Yolu</Form.Label>
                        <Form.File 
                      id="custom-file"
                      label="Custom file input"
                      // onChange={props.handleChange}
                      onChange={handleFileChange}
                      onBlur={props.handleBlur}
                      value={props.values.picture}
                      custom
                      name="picture" 
                    />

                    </Form.Group>
                </Col>
                <Col sm={4} xs={6}>
                <Form.Group controlId="kategori.secimi">
                    <Form.Label>Bağlı Olduğu Kategori</Form.Label>
                    <Form.Control as="select" onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                value={props.values.relatedCategoryId}
                                name="relatedCategoryId" 
                                required>
                                <option selected disabled> Lütfen Seçim Yapınız</option>
                                   {
                                  allCategories.map((item, index) => 
                                    <option  value={item.id} key={index}>{item.mainCat} - {item.title}</option>
                                  )
                                  }

                   
                    </Form.Control>
                </Form.Group>
                </Col>
                <Col sm={2} xs={6}>
                        <Form.Group controlId="order">
                        <Form.Label>Sıra</Form.Label>
                            <Form.Control placeholder="Sıra"
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.categoryOrder}
                                    name="categoryOrder"
                                    // required 
                                    />
                        </Form.Group>
                </Col>
            </Row>
            <Row>
                <Col sm={4}>
                    <Form.Group controlId="featureOneName">
                            <Form.Control placeholder="Beden Adı"
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.featureOneName}
                                    name="featureOneName"
                                    required
                                    />
                        </Form.Group>
                </Col>
                <Col sm={2}>
                    <Form.Group controlId="productFeatures">
                            <Form.Control placeholder="Serideki Adet"
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.featureOneQuantity}
                                    name="featureOneQuantity"
                                    // required
                                    />
                        </Form.Group>
                </Col>
                <Col sm={3}>
                    <Form.Group controlId="imageUrl">
                            <Form.Control placeholder="Stok"
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.featureOneStock}
                                    name="featureOneStock" 
                                    // required
                                    />
                        </Form.Group>
                </Col>
                <Col sm={3}>
                    <Form.Group controlId="imageUrl">
                            <Form.Control placeholder="Birim Fiyat $"
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.featureOnePrice}
                                    name="featureOnePrice" 
                                    // required
                                    />
                        </Form.Group>
                </Col>
            </Row>
            <Row style={{backgroundColor: '#e2e2e2', paddingTop:'5px'}}>
                <Col sm={4}>
                    <Form.Group controlId="productFeatures">
                            <Form.Control placeholder="Beden Adı"
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.productFeatures2Name}
                                    name="featureTwoName"
                                    />
                        </Form.Group>
                </Col>
                <Col sm={2}>
                    <Form.Group controlId="productFeatures">
                            <Form.Control placeholder="Serideki Adet"
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.productFeatures2Quantity}
                                    name="featureTwoQuantity"
                                    // required
                                    />
                        </Form.Group>
                </Col>
                <Col sm={3}>
                    <Form.Group controlId="imageUrl">
                            <Form.Control placeholder="Stok"
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.productFeatures2Stock}
                                    name="featureTwoStock" 
                                    // required
                                    />
                        </Form.Group>
                </Col>
                <Col sm={3}>
                    <Form.Group controlId="imageUrl">
                            <Form.Control placeholder="Birim Fiyat $"
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.productFeatures2Price}
                                    name="featureTwoPrice" 
                                    // required
                                    />
                        </Form.Group>
                </Col>
            </Row>

            <FieldArray
              name="data"
              render={({ insert, remove, push }) => (
                <div xs={12}>
                  {props.values.data.length > 0 &&
                    props.values.data.map((fields, index) => (
                      <div xs={4} className="row" key={index}>
                        <div className="col"> 
                          {/* <label htmlFor={`data.${index}.item`}>- </label> */}
                          <Field
                            name={`data.${index}.item`}
                            // placeholder={field.item}
                            placeholder={fields.item}
                          />
                        </div>
                        <div xs={4}  className="col">
                          <label htmlFor={`data.${index}.value`}>
                            
                          </label>
                          <Field
                            name={`data.${index}.value`}
                            placeholder={fields.value}
                          />
                        </div>
                        <div xs={4}  className="col">
                          <button
                            type="button"
                            className="secondary"
                            onClick={() => remove(index)}
                          >
                            X
                          </button>
                        </div>
                      </div>
                    ))}
                  <button
                    type="button"
                    className="secondary"
                    onClick={() => push({ name: "", value: "" })}
                  >
                    Yeni Ekle
                  </button>
                </div>
              )}
            />
            
            <Button variant="primary float-sm-right" type="submit">Kaydet</Button>
        </Form>
      )}
    </Formik>
  </div>
  </>
);


}
// export default CreateProductForm;
