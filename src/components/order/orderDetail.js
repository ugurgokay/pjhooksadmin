import React, {useEffect,useState} from 'react';
import { Formik } from 'formik';
import axios from 'axios';
import {Form,Button,Row,Col,Table} from 'react-bootstrap';

var eP = require('../../constants.js')


export default function OrderDetail({cats}){

    const [orderDetails, setOrderDetails] = useState()
    const [realFQ, setRealFQ] = useState(0)
    const [realSQ, setRealSQ] = useState(0)

    useEffect(() => {
        setOrderDetails(JSON.parse(cats.orderDetail))
    }, []);


    useEffect(() => {
        let ord = JSON.parse(cats.orderDetail)
        console.log(ord)
        axios.get(eP.apiUrl +  eP.productDetail + '/'+ ord[0].id )
        .then(res => {
            let pT = JSON.parse(res.data[0].productTypes)
            pT.map(node=> { 
                node.id == 1 ? setRealFQ(node.quota) : setRealSQ(node.quota)
            })
        })
    },[orderDetails])

   

    const OrderDetail = ({detail,props}) => {
        let node = JSON.parse(detail)

        return (
            <>
                {node && node.map(item => {
                    return ( 
                        <Row className="m20" key={item.id}>
                            <Col sm={3}>
                                <b>{item.productName} </b><br/>
                                <img src={item.picture} className="img-sml radius10" />
                            </Col>
                            <Col sm={9}>
                                <Table striped bordered hover size="sm">
                                    <thead>
                                        <tr>
                                        <th>#</th>
                                        <th>Seri Adı</th>
                                        <th>Seri Adedi</th>
                                        <th>Birim Fiyat</th>
                                        <th>Toplam Fiyat</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <ProductItems items={item.productTypes} props={props} pId={item.id} />
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>

                    )
                })}
          </>
        )
    }

    const DisplayFormikState = props =>
            <div style={{ margin: '1rem 0', background: '#f6f8fa', padding: '.5rem' }}>
                <strong>Injected Formik props (the form's state)</strong>
                <div style={{}}>
                <code>touched:</code> {JSON.stringify(props.touched, null, 2)}
                </div>
                <div>
                <code>errors:</code> {JSON.stringify(props.errors, null, 2)}
                </div>
                <div>
                <code>values:</code> {JSON.stringify(props.values, null, 2)}
                </div>
                <div>
                <code>isSubmitting:</code> {JSON.stringify(props.isSubmitting, null, 2)}
                </div>
            </div>;


// const getProductItemQuota = (productId,itemId) => {
//     // console.log(productId,itemId)
//     let resultQuota = '';
//     let a = 'kajh'
//     axios.get(eP.apiUrl +  eP.productDetail+ '/'+ productId)
//       .then(function (response) {
//       let prdQuota = JSON.parse(response.data[0].productTypes)

//       prdQuota.map(item => {

//         console.log(item)
//         if(item.id == itemId){
//             console.log(item.quota)
//             resultQuota =  item.quota
//             return resultQuota + '--'
//         }else {
//             return 'nULL'
//         }
//       })
//     //   return resultQuota + 'sasd';

      
//       //   debugger;
//       })
//       .catch(function (error) {
//       console.log('e///', error);
//       alert('Bir Hata Oluştu Lütfen tekrar deneyiniz')
//       //   debugger;
//       });
      
//       return resultQuota + '--'




// }

const ProductItems = ({items,props,pId}) => {
    // console.log(items)
    return (
        <>
        {items && items.map(item => {
            return (
                item.value == "" ? <tr></tr> : (

                <tr key={item.id}> 
                    <td> {item.id} </td>
                    <td> {item.productType}</td>
                    <td> 

                    {/* <Form.Control controlId={pId}
                            type="text"
                            onChange={props.handleChange}
                            onBlur={props.handleBlur}
                            value={pId}
                            /> */}

                        {/* <Form.Group controlId={pId + '-' + item.id + '-' + item.value}> */}
                        {/* <Form.Group controlId={props.values.productDetails}> */}
                        <Form.Group>
                            <Form.Control 
                                    as="select" 
                                    onChange={props.handleChange(pId +';'+item.id)}
                                    onBlur={props.handleBlur}
                                    // value={props.values.productDetails}
                                    required>
                                    <option selected> {item.value}</option>
                                    {
                                    item.quantity.map((item, index) => 
                                        <option  value={item.value} key={index}>{item.value}</option>
                                    )
                                    }

               
                            </Form.Control>
                            </Form.Group>
                         Sipariş Verilen An Stok : {item.quota} <br />
                         <b>Gerçek Stok : {item.id == 1 ? realFQ : realSQ}  </b>
                        {/* { getProductItemQuota(pId,item.id) } */}
                    </td>  
                    <td>{item.productTotalPrice} $</td>
                    <td>{item.productTotalPrice * item.productSubPiece * item.value }  $</td>
                </tr>
                )
            );
          })}
      </>
    )
}


const maintainProductsStock = () => {
    window.location.reload();
}

const _removeOrder = (id) => {

    if (window.confirm("Silmek İstediğinize Emin Misiniz!")) {
    
        axios.put(eP.apiUrl + 'removeOrder/'+ id)
        .then(function (response) {
          console.log(response);
          window.location.reload();
        //   debugger;
        })
        .catch(function (error) {
          console.log('e///', error);
          alert('Bir Hata Oluştu Lütfen tekrar deneyiniz')
        //   debugger;
        });
      } 
}
 

    return(
        <>
            <p> 
                {cats.name}, {cats.createTime} tarihinde sipariş gerçekleştirmiş. 
            </p>
            <hr />
            <h5>Sipariş Detayı</h5>
            <Formik
                initialValues={{ /*productName: '', trialName: ''  productDetails: ''*/}}
                onSubmit={(values, actions) => {
                    setTimeout(() => {
                    // console.log(JSON.stringify(values, null, 2));

                    let objArr = []

                    for (const [key, value] of Object.entries(values)) {
                        // objeyi ayır ve koy içine
                        let splittedArr= key.split(';')
                            splittedArr[2] = value
                            //objeyi oluştur
                            objArr = [...objArr, splittedArr]
                    }


                    orderDetails.map(p => {
                            objArr.forEach((item) => {
                                if(p.id == item[0]){
                                    p.productTypes.map(pT => {
                                    if(pT.id == item[1])
                                        pT.value = item[2]
                                        }) 
                                }
                            })

                        }) 


                    axios.put(eP.apiUrl +  eP.orders+ '/'+ cats.id, {
                          orderDetail: JSON.stringify(orderDetails),
                        })
                        .then(function (response) {
                        console.log(response);
                        maintainProductsStock()
                        //   debugger;
                        })
                        .catch(function (error) {
                        console.log('e///', error);
                        alert('Bir Hata Oluştu Lütfen tekrar deneyiniz')
                        //   debugger;
                        });
                        actions.setSubmitting(false);
                        }, 1000);
                }}
            >
            {props => (
                <>
            <Form onSubmit={props.handleSubmit} className="clearfix">
                <OrderDetail detail={cats.orderDetail} props={props} />
            <Button variant="primary float-sm-right" type="submit">Kaydet</Button>
            {/* <DisplayFormikState {...props} /> */}
            <Button
                    onClick={() => _removeOrder(cats.id) }>
                        Siparişi Sil
                    </Button> 
            </Form>
            </>
      )}
    </Formik>
        </>
    )
}