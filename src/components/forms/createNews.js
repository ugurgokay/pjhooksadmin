import React from 'react';
import { Formik } from 'formik';
import axios from 'axios';
import {Form,Button,Row,Col} from 'react-bootstrap';

var eP = require('../../constants.js')

const CreateNewsForm = () => (
  <div>
    <Formik
      initialValues={{ }}
      onSubmit={(values, actions) => {
        setTimeout(() => {
          console.log(JSON.stringify(values, null, 2));


              console.log(values)

                axios.post(eP.apiUrl +  eP.news, {
                  header: values.header,
                  description: values.description,
                  video: values.video,
                  coverImage: values.coverImage,
                  context : values.context
                })
                .then(function (response) {
                  console.log(response);
                  window.location.reload();
                //   debugger;
                })
                .catch(function (error) {
                  console.log(error);
                  alert('Bir Hata Oluştu Lütfen tekrar deneyiniz')
                //   debugger;
                });

          actions.setSubmitting(false);
        }, 1000);
      }}
    >
      {props => (
        <Form onSubmit={props.handleSubmit} className="clearfix">
            <Row>
                <Col sm={6}>
                    <Form.Group controlId="formHeader">
                        <Form.Label>Başlık</Form.Label>
                        <Form.Control  placeholder="Başlık"
                        type="text"
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        value={props.values.header}
                        name="header" />
                    </Form.Group>
                </Col>
                <Col sm={6}>
                    <Form.Group controlId="formDesccription">
                        <Form.Label>Kısa Açıklama</Form.Label>
                        <Form.Control  placeholder="Kısa Açıklama"
                                type="text"
                                onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                value={props.values.description}
                                name="description"
                                required />
                    </Form.Group>
                </Col>
            </Row>
            <Row>
                <Col xs={6}>
                    <Form.Group controlId="imageUrl">
                        <Form.Label>Video Yolu</Form.Label>
                        <Form.Control placeholder="Video Yolunu Buraya Yapıştırınız"
                                type="text"
                                onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                value={props.values.video}
                                name="video"

                                />
                    </Form.Group>
                </Col>
                <Col xs={6}>
                    <Form.Group controlId="coverImage">
                        <Form.Label>Resim Yolu</Form.Label>
                        <Form.Control placeholder="Resim Yolunu Buraya Yapıştırınız"
                                type="text"
                                onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                value={props.values.coverImage}
                                name="coverImage"
                                required
                                />
                    </Form.Group>
                </Col>
                {/* <Col xs={4}>
                <Form.Group controlId="kategori.secimi">
                    <Form.Label>Ana Kategori</Form.Label>
                    <Form.Control as="select" onChange={props.handleChange}
                                onBlur={props.handleBlur}
                                value={props.values.mainCategoryId}
                                name="mainCategoryId" >
                    <option value="0">Ana Kategori</option>
                    <option value="1">A</option>
                    <option value="2">egori</option>
                    </Form.Control>
                </Form.Group>
                </Col> */}
                <Col xs={6}>
                        <Form.Group controlId="context">
                        <Form.Label>Açıklama</Form.Label>
                            <Form.Control placeholder="Açıklama"
                                    as="textarea"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.context}
                                    name="context"
                                    required />
                        </Form.Group>
                </Col>
            </Row>
            <Button variant="primary float-sm-right" type="submit">Kaydet</Button>
        </Form>
      )}
    </Formik>
  </div>
);
export default CreateNewsForm;
