import React, { Component, useState, useEffect } from 'react';
import axios from 'axios'

import { Button,Modal,Form} from 'react-bootstrap';

import CreateCategoryForm from '../../components/forms/createCategory';

var eP = require('../../constants.js')

// class CategoryList extends Component {
export default function CategoryList({}){


    const [categories, setCategories] = useState();
    const [cats, setCats] = useState([]);
    const [showModal, setShowmOdal] = useState(false);

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    // state = {
    //     cats: [],
    //     showModal: false,
    //   }
      // getInitialState() {
      //   return { showModal: false };
      // }
      // close() {
      //   this.setState({ showModal: false });
      // }
      const open = () =>   {
        setShowmOdal(true);
      }
      
      // TODO : işlemler bitince SWAL falan ekle
// TODO : kategori sıralamaları neye göre olsun ? ordermi belirleyelim yoksa son güncellenenlere mi ?
// TODO  image file upload için bir alan yap copy paste yapılsın imaj yolu kesinliklenn
// create new catteki alt kategorileri ayarlamayı
// TODO alt kategorileri nested birşekilde ver
      // componentDidMount(){
      //   // axios.get('https://jsonplaceholder.typicode.com/posts')

      //   axios.get(eP.apiUrl +  eP.categories)
      //     .then(res => {
      //       console.log(res)
      //       this.setState({
      //           cats:res.data
      //       })
      //     })
      // }
      useEffect(() => {
        axios.get(eP.apiUrl +  eP.categories)
          .then(res => {
            console.log(res)
            setCats(res.data)
          })
      
    }, []);

      // render(){
        // const { cats } = cats;
        const catsList = cats.length ? (
        cats.map(cat => {
          return (
            <div className="post card col-sm-3" key={cat.id}>
              <div className="card-content">
              <img src={cat.image}  style={{height : "135px"}}/>
                <ul className="cat-content">
                  <li> <sup>Adı</sup> {cat.title}</li>
                  {/* <li> <i> Lorem Ipsum sit amei dolor.</i></li> */}
                  {/* <li><br/> <sub>Alt Kategorileri </sub> <br />
                   <code> <span>Gömlek</span>, <span>Ayakkabı</span></code>
                  </li> */}
                </ul>
                <br />
                <sub className="align-sm-right">Sıralama : {cat.categoryOrder}</sub>
              </div>
            </div>
          )        
          })
        ) : (
          <div className="center">No Category Yet</div>
        )
        return(
          <div className="container">
            <Button className="offset-sm-9 col-sm-3" onClick={() => handleShow() } >Alt Kategori Ekle</Button>
            <h4> Kategori Listesi </h4>
            <p>
                <i>
                    Kategoriler ve Alt kategoriler burada görüntülenir
                </i>
            </p>

            <div className="row">
              {catsList}
            </div>
            
            <Modal show={show} onHide={handleClose} size="lg">
              <Modal.Header closeButton>
                <Modal.Title>Yeni Kategori Ekle</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <CreateCategoryForm categories={cats} />
                <hr />
              </Modal.Body>
              <Modal.Footer>
                <Button onClick={() => this.close()}>Close</Button>
              </Modal.Footer>
            </Modal>
        </div>   
    )
  // }
}


// export default CategoryList