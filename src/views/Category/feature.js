import React, { Component } from 'react';
import axios from 'axios'

import {Table,Button} from 'react-bootstrap'
var eP = require('../../constants.js')

class CategoryFeatures extends Component{
    state = {
        feats: []
      }
// const CategoryFeatures = () => {

    componentDidMount(){
        // axios.get('https://jsonplaceholder.typicode.com/posts')
        axios.get(eP.apiUrl +  eP.categoriesFeatures)
          .then(res => {
            console.log(res.data[0].title)
            this.setState({
                feats:res.data.slice(0,10)
            })
          })
      }

    render(){

        const { feats } = this.state;
        const featsList = feats.length ? (
        feats.map(feat => {
            return (
                <tr key={feat.id}>
                    <td>{feat.id}  </td>
                    <td> {feat.title} </td>
                    <td> <span>Gömlek</span>, <span>Ayakkabı</span>  </td>
                    <td> {feat.unit} örn (5 adet) </td>
                </tr>
  
                 
            )        
            })
          ) : (
              <tr>
            <td colSpan="4" className="center">No Category Yet</td>
                </tr>
          )
  



            return( 
                <div className="container">
                    <Button className="offset-sm-9 col-sm-3" onClick={() => this.open() } >Yeni Özellik Ekle</Button>

                    <h4> Kategori Özellikleri </h4>
                    <p>Lipsum</p>
                    <div className="row">
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>Özellik Adı</th>
                            <th>Bağlı Olduğu Kategori(ler)</th>
                            <th>Birim</th>
                            </tr>
                        </thead>
                        <tbody>
                                {featsList}
                        </tbody>
                    </Table>
                    </div>
                </div>   
            )
        }
    }


export default CategoryFeatures;