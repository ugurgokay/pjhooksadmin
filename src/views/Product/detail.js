import React, {useEffect,useState,ChangeEvent} from 'react';
import { Formik,Field,FieldArray } from 'formik';
import axios from 'axios';
import {Form,Button,Row,Table,Col, ToastBody} from 'react-bootstrap';
import {Merge, MergeWith} from "react-lodash"

var eP = require('../../constants.js')
let _ = require('underscore')


export default function CreateProductForm({cats,props}){

  const [prdTypes, setPrdTypes] = useState({})

  // console.log()

  useEffect(() => {
    
    let itm = JSON.parse(cats.productTypes)
    itm = itm[0];
    // console.log(itm.id)
    setPrdTypes(itm);

  }, []);

  

const _removeProduct = () => {
  console.log(cats.id)
  
  if (window.confirm("Silmek İstediğinize Emin Misiniz!")) {
    
    axios.put(eP.apiUrl +  eP.productDetail + '/'+ cats.id)
    .then(function (response) {
      console.log(response);
      window.location.reload();
    //   debugger;
    })
    .catch(function (error) {
      console.log('e///', error);
      alert('Bir Hata Oluştu Lütfen tekrar deneyiniz')
    //   debugger;
    });
  } 

}

    return (
        <>
        <Col sm={3} style={{float:'left'}}>
        <div>Ürün Adı : {cats.productName}</div>
        <img src={cats.picture} style={{width:'100px'}}/>
        </Col>
        <Col sm={9} style={{float:'left'}}>

            <Formik
                    initialValues={{ id : prdTypes.id, productType : prdTypes.productType, productSubPiece: prdTypes.productSubPiece, productTotalPrice: prdTypes.productTotalPrice, quota: prdTypes.quota  }}
                    // initialValues={{ prdName : 'asdfgh' /* JSON.parse(cats.productTypes) */ }}
                    enableReinitialize={true}
                    onSubmit={(values, actions) => {
                        setTimeout(() => {
                        // console.log(JSON.stringify(values, null, 2));
                        console.log('_-___');



                        let subProducts = [];
                        let obj1 = {
                          "id" : 1,
                          "productType":values.productType,
                          "productTotalPrice" : values.productTotalPrice,
                          "productSubPiece" : values.productSubPiece,
                          "quota" : values.quota,
                          "value" : ""
                          // "quantity": subPrOneQuantity
                          }


                          subProducts.push(obj1)


                        axios.put(eP.apiUrl + '_prsdzz/'+ cats.id, {
                            productTypes: JSON.stringify(subProducts),
                            quantity: values.quota,
                            price : values.productTotalPrice
                            })
                            .then(function (response) {
                            console.log(response);
                            window.location.reload();
                            //   debugger;
                            })
                            .catch(function (error) {
                            console.log('e///', error);
                            alert('Bir Hata Oluştu Lütfen tekrar deneyiniz')
                            //   debugger;
                            });
                            actions.setSubmitting(false);
                            }, 1000);
                    }}
                >
                {props => (
                <Form onSubmit={props.handleSubmit} className="clearfix">
                    <Table striped bordered hover size="sm" >
                        
                    <thead>
                            <tr>
                            <th>#</th>
                            <th>Seri Adı</th>
                            <th>Seri Adedi</th>
                            <th>Birim Fiyat</th>
                            <th>Stok </th>
                            </tr>
                        </thead>
                        <tr>
                          <td>#</td>
                          <td>
                          <Form.Group controlId="productType">
                              <Form.Control  placeholder="İngilizce Ürün Adı"
                              type="text"
                              onChange={props.handleChange}
                              onBlur={props.handleBlur}
                              value={props.values.productType}
                              name="productType" />
                          </Form.Group> 
                          </td>
                          <td>
                          <Form.Group controlId="productSubPiece">
                              <Form.Control  placeholder="İngilizce Ürün Adı"
                              type="text"
                              onChange={props.handleChange}
                              onBlur={props.handleBlur}
                              value={props.values.productSubPiece}
                              name="productSubPiece" />
                          </Form.Group> 
                          </td>
                          <td>
                          <Form.Group controlId="productTotalPrice">
                              <Form.Control  placeholder="İngilizce Ürün Adı"
                              type="text"
                              onChange={props.handleChange}
                              onBlur={props.handleBlur}
                              value={props.values.productTotalPrice}
                              name="productTotalPrice" />
                          </Form.Group> 
                          </td>
                          <td>
                          <Form.Group controlId="prdNam">
                              <Form.Control  placeholder="İngilizce Ürün Adı"
                              type="text"
                              onChange={props.handleChange}
                              onBlur={props.handleBlur}
                              value={props.values.quota}
                              name="quota" />
                          </Form.Group> 
                          </td>
                        </tr>
                    </Table>
                <Button variant="primary float-sm-right" type="submit">Kaydet</Button>
                {/* <DisplayFormikState {...props} /> */}

                </Form>
        )}
        </Formik>
        </Col>
        <Col sm={12} style={{clear:'both'}}>
          {/* <Button>Outlete Taşı</Button> */}
          <Button onClick={() =>_removeProduct()}>Ürünü Sil</Button>

        </Col>


        </>
    )


}
