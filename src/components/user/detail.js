import React, {useEffect,useState} from 'react';
import { Formik } from 'formik';
import axios from 'axios';
import {Button} from 'react-bootstrap'

// import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"

var eP = require('../../constants.js')


export default function Useretail({cats}){
    
    let lastLo = cats.location.replace(/"/ig, '');
    let lastLocation = lastLo.replace(';', '%2C')

    let mapSrc = "https://www.google.com/maps/embed/v1/place?q="+lastLocation+"&key=AIzaSyCdg403JbcrcKXQK7yS74pHFYMCsycoQYI"
    useEffect(() => {
        console.log(cats)
    }, []);


    const validateUser = (type) => {


            let typeText = type == 1 ? "Onaylamak" : "Silmek"

        var r = window.confirm("Üyeyi "+  typeText + " istediğinizden emin misiniz? ");
        if (r == true) {
            axios.put(eP.apiUrl +  eP.updateUserStatus+ '/'+ cats.id, {
                status: type
              })
              .then(function (response) {
              console.log(response);
              if(type === 1){
                  alert('Kullanıcı Şifresi : ' + response.data)
              }
              window.location.reload(); 
              })
              .catch(function (error) {
              console.log('e///', error);
              alert('Bir Hata Oluştu Lütfen tekrar deneyiniz')
              //   debugger;
              });
        } else {
        console.log('İPTAL')
        }
    }
   
    
    return(
        <>
            <h5>Üye Detayı</h5>
            <ul>
                <li><b>Ad Soyad :</b> {cats.name} {cats.surname}</li>
                <li> Üyelik Durumu :  {cats.status == 0 ? 'ONAYSIZ' : 'ONAYLI' } 
                    {cats.status == 0 ? <Button onClick={() => validateUser(1)}>Üyeyi Onaylı Yap</Button>  : '' } 
                    <Button className="btn-danger" onClick={() => validateUser(2)}>Üyeyiği İptal et</Button> 
                    <Button className="btn-warning" onClick={() => validateUser(1)}>Yeni Şifre Gönder</Button> 
                 </li>
            </ul>
            <hr />
            <iframe width="600" height="450" src={mapSrc}></iframe>

        </>
    )
}